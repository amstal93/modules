output "swarm_managers_public_ip_addresses" {
  value = aws_instance.manager.*.public_ip
}

output "swarm_managers_aws_hostnames" {
  description = "List of public DNS names assigned to the instances. For EC2-VPC, this is only available if you've enabled DNS hostnames for your VPC"
  value       = aws_instance.manager.*.public_dns
}


output "swarm_managers_eip" {
  description = "Manager elastic ip"
  value       = var.external_eips ? data.aws_eip.eip_manager.*.public_ip : aws_eip.eip_manager.*.public_ip
}

output "swarm_managers_private_ip_addresses" {
  value = aws_instance.manager.*.private_ip
}

output "swarm_manager_tags" {
  description = "List of tags of instances"
  value       = aws_instance.manager.*.tags
}

output "swarm_workers_public_ip_addresses" {
  value = aws_instance.worker.*.public_ip
}

output "swarm_workers_private_ip_addresses" {
  value = aws_instance.worker.*.private_ip
}

output "swarm_workers_aws_hostnames" {
  description = "List of public DNS names assigned to the instances. For EC2-VPC, this is only available if you've enabled DNS hostnames for your VPC"
  value       = aws_instance.worker.*.public_dns
}


output "swarm_worker_tags" {
  description = "List of tags of instances"
  value       = aws_instance.worker.*.tags
}

##################
# Output to files
##################

resource "local_file" "vpc_id" {
  #content  = data.aws_vpc.selected.id
  content  = aws_vpc.vpc.id
  filename = "output/vpc_id.txt"
}

resource "local_file" "region" {
  content  = var.aws_region
  filename = "output/aws_region.txt"
}

resource "local_file" "swarm_security_group" {
  content  = aws_security_group.swarm.id
  filename = "output/swarm-sg_id.txt"
}

resource "local_file" "efs_security_group" {
  count    = var.create_efs_sg ? 1 : 0
  content  = aws_security_group.efs[count.index].id
  filename = "output/efs-sg_id.txt"
}
