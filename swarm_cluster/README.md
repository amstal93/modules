# Swarm cluster Terrform module

Terraform module to deploy AWS infrastructure on AWS for a docker swarm cluster.

## Introduction

This terraform module deploys the needed infrastructure on AWS for a docker swarm cluster that can be provisioned with ansible plabooks and roles.

The manager nodes use elastic IP addresses that can be provisioned by the cluster or use externally provisioned addresses to maintain the cluster public IP addresses. Worker nodes use private addresses only, so the only way to access them is through the manager nodes acting as bastion hosts.

The module configuration is done using tfvars files. As currently there is only one environment we use _terraform.tfvars_. On this file all the customizable values can be found:

  * _environment_: Environment of the cluster.
  * _project_: Project name the cluster belongs to.
  * _manager_count_: Amount of managers nodes (swarm only support odd numbers, so 1, 3, 5 or 7).
  * _worker_count_:  Amount of worker nodes.
  * _manager_instance_type_: EC2 instance type for the mmanager nodes.
  * _worker_instance_type_: EC2 instance type for the worker nodes.
  * _create_nfs_sg_: Configure a security group to enable NFS access on the manager node (default: false).
  * _external_eips_: Use elastic ips externally provisioned (default: false).

For example:

```
environment              = "prod"
project                  = "cluster-swarm"
manager_count            = 1
worker_count             = 2
manager_instance_type    = "t3.small"
worker_instance_type     = "t3.large"
create_nfs_sg            = true
external_eips            = true
```

## Usage

Create a terraform resource file and invoke this module like this:

```
source       = "git::git@gitlab.com:live9/terraform-modules.git///swarm_cluster"
```

For example:


```
module "swarm_cluster" {
  source                   = "git::git@gitlab.com:live9/terraform-modules.git///swarm_cluster"
  environment              = var.environment
  project                  = var.project
  aws_region               = var.aws_region
  manager_count            = var.manager_count
  worker_count             = var.worker_count
  manager_instance_type    = var.manager_instance_type
  worker_instance_type     = var.worker_instance_type
  key_path                 = var.key_path
  private_key_default      = var.key_path_private
  ssh_key_name             = local.ssh_key_name
  create_nfs_sg            = var.create_nfs_sg
  external_eips            = var.external_eips
}
```

Then being on the directory that container the project files, execute the following command:
```
$ terraform init
$ terraform apply
```
About 5-10 minutes later the infrastructure defined by the terraform module will be available. The module will print the access information of the created resources (managers IP, balancer name, etc).

After that the swarm cluster can be provisioned with ansible.
