#!/usr/bin/env bash
#set -x

# Download provision script in case it fails it can be manually run afterwards
curl -s http://169.254.169.254/2009-04-04/user-data > /tmp/provision.sh

{
echo -e "[*] Updating repositories...\n"
sudo apt-get update
[ $? -gt 0 ] && echo -e "ERROR: Could not update system !!\n"
sudo DEBIAN_FRONTEND=noninteractive apt-get install -y  --no-install-recommends \
     python3
[ $? -gt 0 ] && echo -e "ERROR: Could not install base packages !!\n"

# Add swapfile
echo -e "[*] Creating swap file...\n"

# size of swapfile in megabytes
swap_size=$(grep '^MemTotal:' /proc/meminfo|awk '{ print $2 }')
swap_file="/var/swapfile"

# does the swap file already exist?
grep -q "swapfile" /etc/fstab
if [ $? -ne 0 ]; then
	echo 'swapfile not found. Adding swapfile.'
	fallocate -l ${swap_size}K ${swap_file}
	chmod 600 ${swap_file}
	mkswap ${swap_file}
	swapon ${swap_file}
	echo ${swap_file} none swap defaults 0 0 >> /etc/fstab
else
	echo 'swapfile found. No changes made.'
fi

touch /tmp/provisioned
echo -e "[*] Provisioning finished.\n"
} > /tmp/provision.txt 2>/tmp/provision.err
