resource "aws_eip" "eip_manager" {
  instance = aws_instance.manager.*.id[count.index]
  count    = var.external_eips ? 0 : var.manager_count
  vpc      = true
  tags = {
    Name = "eip-${var.environment}-${count.index + 1}"
  }
}


data "aws_eip" "eip_manager" {
  count = var.external_eips ? var.manager_count : 0
  tags = {
    Name = "eip-${var.environment}-${count.index + 1}"
  }
}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.manager.*.id[count.index]
  count         = var.external_eips ? var.manager_count : 0
  allocation_id = data.aws_eip.eip_manager.*.id[count.index]
}
