resource "aws_key_pair" "default" {
  key_name   = var.ssh_key_name
  public_key = file(var.key_path)
}

resource "aws_instance" "manager" {
  count                  = var.manager_count
  ami                    = var.manager_ami
  instance_type          = var.manager_instance_type
  iam_instance_profile   = aws_iam_instance_profile.ec2-storage-instance-profile.name
  key_name               = aws_key_pair.default.id
  user_data              = file("${path.module}/${var.bootstrap_path}")
  vpc_security_group_ids = var.create_nfs_sg == true ? concat([aws_security_group.swarm.id], [aws_security_group.nfs[0].id]) : [aws_security_group.swarm.id]
  subnet_id              = element(aws_subnet.public.*.id, count.index)
  #associate_public_ip_address  = false

  root_block_device {
    volume_size           = var.manager_disk_size
    volume_type           = var.manager_disk_type
    delete_on_termination = "true"
  }

  tags = {
    Name        = format("swarm-manager-%s-%d", var.environment, count.index + 1)
    manager     = true
    environment = var.environment
    project     = var.project
    group       = "swarm"
  }
}

# Currently destroy-provisioners are buggy and not always work due to the
# following terraform bug:
# https://github.com/hashicorp/terraform/issues/13549
# Our bug report for more details:
# https://gitlab.com/live9/terraform/modules/-/issues/1

# Begin detroy-provisioners
resource "null_resource" "cleanup_managers" {
  lifecycle {
    create_before_destroy = true
  }

  triggers = {
    instance_ip_addr = var.external_eips == true ? data.aws_eip.eip_manager[0].public_ip : aws_eip.eip_manager[0].public_ip
    user             = var.ssh_user
    private_key      = var.private_key_default
  }

  provisioner "remote-exec" {
    when       = destroy
    on_failure = continue

    inline = [
      "/usr/local/bin/destroy.sh",
    ]

    connection {
      type        = "ssh"
      user        = self.triggers.user
      private_key = file(self.triggers.private_key)
      host        = self.triggers.instance_ip_addr
    }
  }
}

resource "null_resource" "cleanup_workers" {
  count = var.worker_count
  lifecycle {
    create_before_destroy = true
  }
  triggers = {
    instance_ip_addr   = var.external_eips == true ? data.aws_eip.eip_manager[0].public_ip : aws_eip.eip_manager[0].public_ip
    user               = var.ssh_user
    private_key        = var.private_key_default
    num_workers        = var.worker_count
    private_ip_address = aws_instance.worker[count.index].private_ip
  }

  provisioner "remote-exec" {
    when       = destroy
    on_failure = continue

    inline = [
      "/usr/local/bin/destroy.sh",
    ]

    connection {
      type                = "ssh"
      user                = self.triggers.user
      private_key         = file(self.triggers.private_key)
      host                = self.triggers.private_ip_address
      bastion_user        = self.triggers.user
      bastion_host        = self.triggers.instance_ip_addr
      bastion_private_key = file(self.triggers.private_key)
    }
  }
}
# End destroy-provisioners

resource "aws_instance" "worker" {
  count                       = var.worker_count
  ami                         = var.worker_ami
  instance_type               = var.worker_instance_type
  iam_instance_profile        = aws_iam_instance_profile.ec2-storage-instance-profile.name
  key_name                    = aws_key_pair.default.id
  user_data                   = file("${path.module}/${var.bootstrap_path}")
  vpc_security_group_ids      = [aws_security_group.swarm.id]
  subnet_id                   = element(aws_subnet.private.*.id, count.index)
  associate_public_ip_address = var.workers_public_ip
  depends_on                  = [aws_nat_gateway.nat_gateway]

  root_block_device {
    volume_size           = var.worker_disk_size
    volume_type           = var.worker_disk_type
    delete_on_termination = "true"
  }

  connection {
    type        = "ssh"
    user        = var.ssh_user
    private_key = file(var.private_key_default)
    host        = self.public_ip
  }

  tags = {
    Name        = format("swarm-worker-%s-%d", var.environment, count.index + 1)
    worker      = true
    environment = var.environment
    project     = var.project
    group       = "swarm"
  }
}
