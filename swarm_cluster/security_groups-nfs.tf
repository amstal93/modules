resource "aws_security_group" "nfs" {
  count  = var.create_nfs_sg ? 1 : 0
  name   = "nfs-${var.environment}"
  vpc_id = aws_vpc.vpc.id

  ingress {
    from_port       = 2049
    to_port         = 2049
    protocol        = "tcp"
    self            = true
    security_groups = [aws_security_group.swarm.id]
  }

  ingress {
    from_port       = 111
    to_port         = 111
    protocol        = "tcp"
    self            = true
    security_groups = [aws_security_group.swarm.id]
  }

  ingress {
    from_port       = 32768
    to_port         = 32768
    protocol        = "tcp"
    self            = true
    security_groups = [aws_security_group.swarm.id]
  }

  ingress {
    from_port       = 44182
    to_port         = 44182
    protocol        = "tcp"
    self            = true
    security_groups = [aws_security_group.swarm.id]
  }

  ingress {
    from_port       = 54508
    to_port         = 54508
    protocol        = "tcp"
    self            = true
    security_groups = [aws_security_group.swarm.id]
  }

  ingress {
    from_port       = 32770
    to_port         = 32800
    protocol        = "tcp"
    self            = true
    security_groups = [aws_security_group.swarm.id]
  }

  # UDP
  ingress {
    from_port       = 2049
    to_port         = 2049
    protocol        = "udp"
    self            = true
    security_groups = [aws_security_group.swarm.id]
  }

  ingress {
    from_port       = 111
    to_port         = 111
    protocol        = "udp"
    self            = true
    security_groups = [aws_security_group.swarm.id]
  }

  ingress {
    from_port       = 32768
    to_port         = 32768
    protocol        = "udp"
    self            = true
    security_groups = [aws_security_group.swarm.id]
  }

  /*
ingress {
  from_port   = 44182
  to_port     = 44182
  protocol    = "udp"
  self        = true
  security_groups = [aws_security_group.swarm.id]
}

ingress {
  from_port   = 54508
  to_port     = 54508
  protocol    = "udp"
  self        = true
  security_groups = [aws_security_group.swarm.id]
}
*/

  ingress {
    from_port       = 32770
    to_port         = 32800
    protocol        = "udp"
    self            = true
    security_groups = [aws_security_group.swarm.id]
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "nfs-${var.environment}"
    environment = var.environment
  }

}
