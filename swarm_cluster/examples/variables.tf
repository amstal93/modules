variable environment {}
variable project {}
variable aws_region {}
variable manager_count {}
variable worker_count {}
variable manager_instance_type {}
variable worker_instance_type {}
variable key_path {}
variable key_path_private {}
variable create_nfs_sg {}
variable external_eips {}

locals {
  ssh_key_name = ""
}
