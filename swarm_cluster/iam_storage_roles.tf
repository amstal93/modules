resource "aws_iam_role" "ec2-storage-role" {
  name               = "ec2-storage-role-${var.environment}"
  assume_role_policy = file("${path.module}/iam_policies/assume_role_policy.json")
}

resource "aws_iam_policy" "efs-policy" {
  name        = "efs-policy-${var.environment}"
  description = "Permisos de acceso a EFS"
  policy      = file("${path.module}/iam_policies/efs.json")
}

resource "aws_iam_policy" "s3-policy" {
  name        = "s3-policy-${var.environment}"
  description = "Permisos de acceso para s3"
  policy      = file("${path.module}/iam_policies/s3.json")
}

resource "aws_iam_role_policy_attachment" "efs-attach" {
  role       = aws_iam_role.ec2-storage-role.name
  policy_arn = aws_iam_policy.efs-policy.arn
}

resource "aws_iam_role_policy_attachment" "s3-attach" {
  role       = aws_iam_role.ec2-storage-role.name
  policy_arn = aws_iam_policy.s3-policy.arn
}

resource "aws_iam_instance_profile" "ec2-storage-instance-profile" {
  name = "ec2-storage-instance-profile-${var.environment}"
  role = aws_iam_role.ec2-storage-role.name
}
