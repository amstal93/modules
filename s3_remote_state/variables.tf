variable "aws_region" {
  description = "AWS region on which we will setup the swarm cluster"
  default     = "us-east-2"
}

variable "environment" {
  description = "Environment name"
}

variable "project" {
  description = "Project name"
}

variable "force_destroy" {
  description = "Delete objects from bucket when its not empty"
  default     = false
}
