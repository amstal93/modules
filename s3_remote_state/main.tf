resource "aws_s3_bucket" "terraform_state" {
  bucket        = "${var.project}-terraform-state-${var.environment}" # Enable versioning so we can see the full revision history of our
  force_destroy = var.force_destroy
  # state files
  versioning {
    enabled = true
  } # Enable server-side encryption by default
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  tags = {
    name = "${var.project}-remote_state-${var.environment}"
    env  = "${var.environment}"
  }
}

resource "aws_s3_bucket_public_access_block" "terraform_state" {
  bucket = aws_s3_bucket.terraform_state.id

  block_public_acls   = true
  block_public_policy = true
}

resource "aws_dynamodb_table" "terraform_locks" {
  name         = "${var.project}-terraform-locks-${var.environment}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}
