# s3 buckets for Terraform remote states

All modules store their terraform state [remotely in a AWS s3 bucket per environment](https://blog.gruntwork.io/how-to-manage-terraform-state-28f5697e68fa). The s3 buckets are created using terraform too, and then their own state is stored in the s3 bucket.

## Usage
### Creation of s3 buckets

First we need to create a s3 bucket per environment and then configure them to store their own remote states in each of them. This means that the `terraform` block in `main.tf` must be commented when the buckets are being created as does not exists yet. Then those buckets can be used to store the remote state of themselves and all modules of each environment.

To create the s3 buckets go to each environment's `s3_remote_state` project and run:

```
cd prod/s3_remote_state
terraform init
terraform apply
```

Then, in `main.tf` uncomment the first lines that configure the project to use a remote state:

```
terraform {
  backend "s3" {
    bucket         = "karisma-terraform-state-prod"
    key            = "s3/terraform.tfstate"
    region         = "us-east-2"    # Replace this with your DynamoDB table name!
    dynamodb_table = "terraform-locks-prod"
    encrypt        = true
    profile        = "karisma"
  }
}
```

Then run `terraform init` again so the state file is uploaded to the s3 bucket. Each project on each environment is already configured to use remote states so there is nothing left to do.

### Deletion of s3 buckets
When destroying you have to do the opposite: Comment out the `terraform` block and run `terraform init` so the state file is downloaded and the remote state backend is unconfigured, then `terraform destroy` can be run.

## Examples
For a complete example see the [examples](examples) directory.
