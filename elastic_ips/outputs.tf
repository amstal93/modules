output "swarm_managers_eips" {
  description = "Manager elastic ips"
  value       = aws_eip.eip.*.public_ip
}
