variable "aws_region" {
  description = "AWS region on which we will setup the swarm cluster"
  default     = "us-east-2"
}

variable "environment" {
  description = "Environment name"
  default     = "preprod"
}

variable "eip_count" {
  description = "Amount of elastic IP addresses to create"
  default     = "1"
}
