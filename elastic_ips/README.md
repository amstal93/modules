# Elastic IPs module

This module create elastic IP addresses inside a VPC that can be used with other terraform module like our [Swarm Cluster](https://gitlab.com/live9/terraform-modules/-/tree/master/swarm_cluster).

The module has the following variables:

* _aws_region_: AWS region on which we will setup the swarm cluster (default: us-east-2).
* _environment_: Environment name (default: preprod).
* _eip_count_: Amount of elastic IP addresses to create (default: 1).

## Usage

In your project create a module definition like this one:

```
module "external_eips" {
  source                = "git::git@gitlab.com:live9/terraform-modules.git///elastic_ips"
  environment           = "staging"
  aws_region            = "us-east-1"
  eip_count             = "1"
}
```

Then run:

```
terraform apply
```
### Outputs
* _Swarm_managers_eips_: The elastic ips values.

The elastic ips will be created with a tag called `Name` with the format `eip-${env}-${count}`. For example, if the `environment` variable is set to "staging" and `eip_count` is set to 2, the tags will be `eip-staging-1` and `eip-staging-2`. Then where you need them use [aws_eip data sources](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eip) to fetch them using the tag `Name`:

```
data "aws_eip" "eip_manager" {
  tags = {
    Name = "eip-${var.environment}-${count.index + 1}"
  }
}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.manager.*.id[count.index]
  allocation_id = data.aws_eip.eip_manager.*.id[count.index]
}
```

## Examples
For a complete example see the [examples](examples) directory.
