variable "environment" {}
variable "aws_profile" {}
variable "aws_region" {}
variable "project" {}
variable "key_path" {}
variable "key_path_private" {}
variable "ssh_key_name" {}
