module "external_eips" {
  source                = "../"
  environment           = var.environment
  aws_region            = var.aws_region
}
