# Terraform modules

This repository contains some terraform modules to build AWS infrastructure. This is the list of modules at the moment:

* swarm_cluster: Module to create AWS infrastruture to deploy a docker swarm cluster. See the module's README file for instructions.
* elastic_ips: Module to create a set of AWS elastic IPs that can be used with other modules, like the swarm_cluster module. See the module's README file for instructions.

